-- tofix: player models animations, HUD being removed, reasonable sudden_death

local modname = "block_bomber_mg"
blockbomber = {}
blockbomber.modname = modname
local S = core.get_translator(blockbomber.modname)


arena_lib.register_minigame(modname,{
    name = S("Blockbomber"),
    prefix = "BB: ",
    icon = "blbmr_icon.png",
    -- min_version = 43,
    player_aspect = {
        textures = {"blank.png","blank.png"},
        collisionbox = {-0.4, 0.0, -0.4, 0.4, 0.9,0.4},
        stepheight = 0.3,
        eye_height = 1.3,
        physical = false,
        collides_with_objects = false,
    },
    hud_flags = {wielditem=false,healthbar=false,crosshair=true,breathbar=false,minimap=true},
    damage_modifiers = {fall_damage_add_percent=-100},
    -- camera_offset = {},
    hotbar = {
        slots = 2,
        background_image = "bb_player_gui_hotbar_2.png",
        selected_image = "blank.png",
    },
    load_time = 6,
    min_players = 2,
    regenerate_map = true,
    disable_inventory = true,
    show_nametags = true,
    time_mode = "incremental",
    in_game_physics = {
        -- physics override parameters
        speed = 0,
        gravity = 70,
        jump = 0,
        sneak = false,
        sneak_glitch = false,
        new_move = true,
    },
    disabled_damage_types = {"fall","punch"},
    properties = {
        sudden_death_time = 180,
        origin = {x=0,y=0,z=0}, -- this will be set to a position inside the arena arena; where the schematic is placed and the offset for player spawns
    },
    temp_properties = {
        match_id = 0,
        countdown = -1,
        sudden_death = false,
        sudden_death_timer = 0,
        map = {},
        is_loaded = false,
    },
    player_properties = {
        speed = 0,
        dead = false,
        cause = "",
        anim = "stand",
        -- original_attached = false,
    },
})

local path = core.get_modpath(modname)

-- // load files from the src directory

local files = {
    "storage",
    "editor",
    "bombs",
    "controls",
    "deathmessages",
    "globalstep",
    "hud",
    "items",
    "minigame_manager",
    "misc_entities",
    "nodes",
    "player_colors",
    "sudden_death"
}



for _, item in ipairs(files) do
    dofile(path .. DIR_DELIM .. "src" .. DIR_DELIM .. item .. ".lua")
end
