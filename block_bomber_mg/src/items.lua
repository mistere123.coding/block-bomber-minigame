local S = core.get_translator(blockbomber.modname)

-- +1 powerups

core.register_craftitem(":bb_powerup:extra_bomb", {
    description = S("Extra Bomb"),
    inventory_image = "bb_powerup_bomb.png",
    on_drop = function() return end,
    groups = {good = 1},
})

core.register_craftitem(":bb_powerup:extra_life", {
    description = S("Extra Life"),
    inventory_image = "bb_powerup_extra_life.png",
    on_drop = function(itemstack) return itemstack end,
    groups = {good = 1},
})

core.register_craftitem(":bb_powerup:extra_speed", {
    description = S("Extra Speed"),
    inventory_image = "bb_powerup_fast.png",
    on_drop = function(itemstack) return itemstack end,
    groups = {good = 1},
})

core.register_craftitem(":bb_powerup:extra_power", {
    description = S("Extra Power"),
    inventory_image = "bb_powerup_power.png",
    on_drop = function(itemstack) return itemstack end,
    groups = {good = 1},
})

-- permanent powerups

core.register_craftitem(":bb_powerup:throw", {
    description = S("Throw Bombs"),
    inventory_image = "bb_powerup_throw.png",
    on_drop = function(itemstack) return itemstack end,
    groups = {good = 1},
})

core.register_craftitem(":bb_powerup:kick", {
    description = S("Kick Bombs"),
    inventory_image = "bb_powerup_kick.png",
    on_drop = function(itemstack) return itemstack end,
    groups = {good = 1},
})

core.register_tool(":bb_powerup:multidir", {
    description = S("Multidir"),
    inventory_image = "bb_powerup_multidir.png",
    on_drop = function(itemstack) return itemstack end,
    groups = {good = 1},
})

-- neutral modifiers

core.register_tool(":bb_powerup:superfast", {
    description = S("Superfast"),
    inventory_image = "bb_powerup_superfast.png",
    on_drop = function(itemstack) return itemstack end,
    groups = {bad = 1},
})

-- bad modifiers

core.register_tool(":bb_powerup:slow", {
    description = S("Slow"),
    inventory_image = "bb_powerup_slow.png",
    on_drop = function(itemstack) return itemstack end,
    groups = {bad = 1},
})

core.register_tool(":bb_powerup:weird_control", {
    description = S("Weird Control"),
    inventory_image = "bb_powerup_weird_control.png",
    on_drop = function(itemstack) return itemstack end,
    groups = {bad = 1},
})

core.register_tool(":bb_powerup:no_placebomb", {
    description = S("No Place Bomb"),
    inventory_image = "bb_powerup_no_placebomb.png",
    on_drop = function(itemstack) return itemstack end,
    groups = {bad = 1},
})

core.register_tool(":bb_powerup:party", {
    description = S("Party: Causes Bad Cases of Bomb Placement"),
    inventory_image = "bb_powerup_party.png",
    on_drop = function(itemstack) return itemstack end,
    groups = {bad = 1},
})

core.register_item("block_bomber_mg:bb_hand", {
    type = "none",
    range = 1.0,
    groups = {not_in_creative_inventory = 1},
})