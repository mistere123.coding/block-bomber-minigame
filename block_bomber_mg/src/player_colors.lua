local S = core.get_translator(blockbomber.modname)
local player_colors_avail = {
    "#472d3c",
    "#5e3643",
    "#7a444a",
    "#a05b53",
    "#bf7958",
    "#eea160",
    "#b6d53c",
    "#71aa34",
    "#397b44",
    "#3c5956",
    "#5a5353",
    "#dff6f5",
    "#8aebf1",
    "#28ccdf",
    "#3978a8",
    "#394778",
    "#39314b",
    "#564064",
    "#8e478c",
    "#cd6093",
    "#ffaeb6",
    "#f4b41b",
    "#f47e1b",
    "#e6482e",
    "#a93b3b",
    "#827094",
    "#4f546b",
}


function blockbomber.set_textures(player)
    local p_name = player:get_player_name()
    player:set_properties({textures={"white.png^[colorize:"..blockbomber.player_colors[p_name]..":255","player_parts.png"}})

end


function blockbomber.set_ghost_textures(player)
    local p_name = player:get_player_name()
    player:set_properties({textures={"white.png^[colorize:"..blockbomber.player_colors[p_name]..":127","player_parts.png"}})
end

core.register_on_joinplayer(function(player)
    local p_name = player:get_player_name()
    if not blockbomber.player_colors[p_name] then
        local randcolor = player_colors_avail[math.random(1,#player_colors_avail)]
        blockbomber.player_colors[p_name] = randcolor
        blockbomber.save_player_colors()
    end
    
end)