local S = core.get_translator(blockbomber.modname)
local bb_nodes = {}

bb_nodes.slope_box = {
	type = "fixed",
	fixed = {
		{-0.5000, -0.5000, 0.5000, 0.5000, 0.5000, 0.3750},
		{-0.5000, -0.5000, 0.3750, 0.5000, 0.3750, 0.2500},
		{-0.5000, -0.5000, 0.2500, 0.5000, 0.2500, 0.1250},
		{-0.5000, -0.5000, 0.1250, 0.5000, 0.1250, 0.000},
		{-0.5000, -0.5000, -0.000, 0.5000, 0.000, -0.1250},
		{-0.5000, -0.5000, -0.1250, 0.5000, -0.1250, -0.2500},
		{-0.5000, -0.5000, -0.2500, 0.5000, -0.2500, -0.3750},
		{-0.5000, -0.5000, -0.3750, 0.5000, -0.3750, -0.5000}
	}
}
bb_nodes.inner_slope_box = {
	type = "fixed",
	fixed = {
		{-0.5000, -0.5000, 0.5000, 0.5000, 0.5000, 0.3750},
		{-0.5000, -0.5000, 0.3750, 0.5000, 0.3750, 0.2500},
		{-0.5000, -0.5000, 0.2500, 0.5000, 0.2500, 0.1250},
		{-0.5000, -0.5000, 0.1250, 0.5000, 0.1250, 0.000},
		{-0.5000, -0.5000, -0.000, 0.5000, 0.000, -0.1250},
		{-0.5000, -0.5000, -0.1250, 0.5000, -0.1250, -0.2500},
		{-0.5000, -0.5000, -0.2500, 0.5000, -0.2500, -0.3750},
		{-0.5000, -0.5000, -0.3750, 0.5000, -0.3750, -0.5000},

        {-0.5000, -0.5000, -0.5000,   -0.3750  , 0.5000 ,0.5000},
		{-0.5000, -0.5000, -0.3750,   -0.2500  , 0.3750 ,0.5000},
		{-0.5000, -0.5000, -0.2500,   -0.1250  , 0.2500 ,0.5000},
		{-0.5000, -0.5000, -0.1250,   -0.000   , 0.1250 ,0.5000},
		{-0.5000, -0.5000, 0.000,   0.1250 , 0.000  ,0.5000},
		{-0.5000, -0.5000, 0.1250,  0.2500 , -0.1250, 0.5000},
		{-0.5000, -0.5000, 0.2500,  0.3750 , -0.2500,0.5000},
		{-0.5000, -0.5000, 0.3750,  0.5000 , -0.3750,0.5000}
	}
}


bb_nodes.outer_slope_box = {
	type = "fixed",
	fixed = {
		{0.5000, -0.5000, 0.5000, 0.3750, 0.5000, 0.3750},
		{0.3750, -0.5000, 0.5000, 0.2500, 0.3750, 0.3750},
		{0.5000, -0.5000, 0.3750, 0.3750, 0.3750, 0.2500},
		{0.2500, -0.5000, 0.5000, 0.1250, 0.2500, 0.3750},
		{0.3750, -0.5000, 0.3750, 0.2500, 0.2500, 0.2500},
		{0.5000, -0.5000, 0.2500, 0.3750, 0.2500, 0.1250},
		{0.1250, -0.5000, 0.5000, 0.000, 0.1250, 0.3750},
		{0.2500, -0.5000, 0.3750, 0.1250, 0.1250, 0.2500},
		{0.3750, -0.5000, 0.2500, 0.2500, 0.1250, 0.1250},
		{0.5000, -0.5000, 0.1250, 0.3750, 0.1250, 0.000},
		{-0.000, -0.5000, 0.5000, -0.1250, 0.000, 0.3750},
		{0.1250, -0.5000, 0.3750, 0.000, 0.000, 0.2500},
		{0.2500, -0.5000, 0.2500, 0.1250, 0.000, 0.1250},
		{0.3750, -0.5000, 0.1250, 0.2500, 0.000, 0.000},
		{0.5000, -0.5000, 0.000, 0.3750, 0.000, -0.1250},
		{0.1250, -0.5000, 0.5000, -0.2500, -0.1250, 0.3750},
		{0.000, -0.5000, 0.3750, -0.1250, -0.1250, 0.2500},
		{0.1250, -0.5000, 0.2500, 0.000, -0.1250, 0.1250},
		{0.2500, -0.5000, 0.1250, 0.1250, -0.1250, 0.000},
		{0.3750, -0.5000, 0.000, 0.2500, -0.1250, -0.1250},
		{0.5000, -0.5000, -0.1250, 0.3750, -0.1250, -0.2500},
		{-0.2500, -0.5000, 0.5000, -0.3750, -0.2500, 0.3750},
		{-0.1250, -0.5000, 0.3750, -0.2500, -0.2500, 0.2500},
		{-0.000, -0.5000, 0.2500, -0.1250, -0.2500, 0.1250},
		{0.1250, -0.5000, 0.1250, 0.000, -0.2500, 0.000},
		{0.2500, -0.5000, 0.000, 0.1250, -0.2500, -0.1250},
		{0.3750, -0.5000, -0.1250, 0.2500, -0.2500, -0.2500},
		{0.5000, -0.5000, -0.2500, 0.3750, -0.2500, -0.3750},
		{-0.3750, -0.5000, 0.5000, -0.5000, -0.3750, 0.3750},
		{-0.2500, -0.5000, 0.3750, -0.3750, -0.3750, 0.2500},
		{-0.1250, -0.5000, 0.2500, -0.2500, -0.3750, 0.1250},
		{-0.000, -0.5000, 0.1250, -0.1250, -0.3750, 0.000},
		{0.1250, -0.5000, 0.000, 0.000, -0.3750, -0.1250},
		{0.2500, -0.5000, -0.1250, 0.1250, -0.3750, -0.2500},
		{0.3750, -0.5000, -0.2500, 0.2500, -0.3750, -0.3750},
		{0.5000, -0.5000, -0.3750, 0.3750, -0.3750, -0.5000}
	}
}


-- core.register_tool("bb_nodes:breaker", {
--     description = "Node Breaker",
--     inventory_image = "bb_nodes_breaker.png",
--     tool_capabilities = {
--         full_punch_interval = 1.5,
--         max_drop_level = 1,
--         groupcaps = {
--             cracky = {
--                 maxlevel = 3,
--                 uses = 2000,
--                 times = { [1]=0.20, [2]=0.20, [3]=0.20 }
--             },
--         },
--     },
-- })



function bb_nodes.register_floor_set(name, desc, texture)
    core.register_node(":bb_nodes:"..name.."_slope", {
        description = S("@1 Slope", desc),
        paramtype = "light",
        drawtype = "mesh",
        mesh = "slope.obj",
        node_box = bb_nodes.slope_box,
        paramtype2 = "facedir",
        selection_box = bb_nodes.slope_box,
        collision_box = bb_nodes.slope_box,
        tiles = {texture},
        groups = {cracky = 3, floor = 1, solid = 1},
    })

    core.register_node(":bb_nodes:"..name, {
        description = S("@1", desc),
        paramtype = "light",
        tiles = {texture},
        groups = {cracky = 3, floor = 1, solid = 1},
    })

    core.register_node(":bb_nodes:"..name.."_slope_inner", {
        description = S("@1 Inner Slope", desc),
        paramtype = "light",
        drawtype = "mesh",
        mesh = "slope_inside.obj",
        paramtype2 = "facedir",
        selection_box = bb_nodes.inner_slope_box,
        collision_box = bb_nodes.inner_slope_box,
        tiles = {texture},
        groups = {cracky = 3, floor = 1, solid = 1},
    })

    core.register_node(":bb_nodes:"..name.."_slope_outer", {
        description = S("@1 Outer Slope", desc),
        paramtype = "light",
        drawtype = "mesh",
        mesh = "slope_outside.obj",
        paramtype2 = "facedir",
        selection_box = bb_nodes.outer_slope_box,
        collision_box = bb_nodes.outer_slope_box,
        tiles = {texture},
        groups = {cracky = 3, floor = 1, solid = 1},
    })
end

-- Classic set
bb_nodes.register_floor_set("floor", S("Floor"), "bb_nodes_floor.png")

core.register_node(":bb_nodes:floor_with_arrow", {
    description = S("Floor Arrow\nPushes bombs in this direction"),
    tiles = {"bb_nodes_floor_with_arrow.png", "bb_nodes_floor.png"},
    groups = {cracky = 3, floor = 1, arrow = 1, solid = 1},
    paramtype = "light",
    paramtype2 = "facedir",
})

core.register_node(":bb_nodes:floor_with_mortar", {
    description = S("Floor Mortar\nCollects and randomly throws bombs"),
    tiles = {"bb_nodes_floor_with_mortar.png", "bb_nodes_floor.png"},
    groups = {cracky = 3, floor = 1, mortar = 1, solid = 1},
    paramtype = "light",
    paramtype2 = "facedir",
})

core.register_node(":bb_nodes:brick", {
    description = S("Removable Wall"),
    tiles = {"bb_nodes_brick.png^[transformFX", "bb_nodes_brick.png"},
    paramtype = "light",
    groups = {cracky = 3, penetrable = 1, removable = 1, powerup = 1, solid = 1},
    collision_box = {
        type = "fixed",
        fixed = {
            { -0.5, -.5, -0.5, 0.5, 2.0, 0.5 },
            { -0.5, -.5, -0.5, 0.5, .5, 0.5 },
        } -- Makes it 2 nodes high
    },
})

core.register_node(":bb_nodes:crate", {
    description = S("Removable Crate"),
    tiles = {"bb_nodes_crate.png"},
    paramtype = "light",
    groups = {cracky = 3, penetrable = 1, removable = 1, powerup = 2, solid = 1},
    collision_box = {
        type = "fixed",
        fixed = {
            { -0.5, -.5, -0.5, 0.5, 2.0, 0.5 },
            { -0.5, -.5, -0.5, 0.5, .5, 0.5 },
        } -- Makes it 2 nodes high
    },
})

core.register_node(":bb_nodes:ice", {
    description = S("Slippery Ice"),
    tiles = {"bb_nodes_ice.png^[opacity:200"},
    paramtype = "light",
    use_texture_alpha = "blend",
    drawtype = "glasslike_framed_optional",
    groups = {cracky = 3, slippery = 9, floor = 1, solid = 1},
})

core.register_node(":bb_nodes:wall", {
    description = S("Solid Wall"),
    tiles = {"bb_nodes_wall.png"},
    paramtype = "light",
    groups = {cracky = 3, penetrable = 1, solid = 1},
    collision_box = {
        type = "fixed",
        fixed = {
            { -0.5, -.5, -0.5, 0.5, 2.0, 0.5 },
            { -0.5, -.5, -0.5, 0.5, .5, 0.5 },
        } -- Makes it 2 nodes high
    },
})

core.register_node(":bb_nodes:fence", {
    description = S("Fence"),
    paramtype = "light",
    drawtype = "mesh",
    mesh = "fence.obj",
    paramtype2 = "facedir",
    selection_box = {
        type = "fixed",
        fixed = {
            {-0.5000, -0.5000, 0.5000, 0.5000, 0.5000, 0.3750}
        },
    },
    collision_box = {
        type = "fixed",
        fixed = {
            {-0.5000, -0.5000, 0.5000, 0.5000, 0.5000, 0.3750},
            {-0.5000, -0.5000, 0.5000, 0.5000, 1.9, 0.3750},
        },
    },
    tiles = {"fence_classic.png"},
    groups = {cracky = 3, penetrable = 1, solid = 1},
})

core.register_node(":bb_nodes:fence_corner", {
    description = S("Fence (corner)"),
    paramtype = "light",
    drawtype = "mesh",
    mesh = "fence_corner.obj",
    paramtype2 = "facedir",
    selection_box = {
        type = "fixed",
        fixed = {
            {-0.5000, -0.5000, 0.5000, 0.5000, 0.5000, 0.3750},
            {0.5000, 0.5000, -0.5000, 0.3750, -0.5000, 0.5000}
        },
    },
    collision_box = {
        type = "fixed",
        fixed = {
            {-0.5000, -0.5000, 0.5000, 0.5000, 0.5000, 0.3750},
            {0.5000, 0.5000, -0.5000, 0.3750, -0.5000, 0.5000},
            {-0.5000, -0.5000, 0.5000, 0.5000, 1.9, 0.3750},
            {0.5000, 1.9, -0.5000, 0.3750, -0.5000, 0.5000}
        },
    },
    tiles = {"fence_classic.png"},
    groups = {cracky = 3, penetrable = 1, solid = 1},
})

-- Forest set
bb_nodes.register_floor_set("forest_floor", S("Forest Floor"), "bb_nodes_floor_forest.png")

core.register_node(":bb_nodes:forest_floor_with_arrow", {
    description = S("Forest Floor Arrow\nPushes bombs in this direction"),
    tiles = {"bb_nodes_floor_with_arrow_forest.png", "bb_nodes_floor_forest.png"},
    groups = {cracky = 3, floor = 1, arrow = 1, solid = 1},
    paramtype = "light",
    paramtype2 = "facedir",
})

core.register_node(":bb_nodes:forest_floor_with_mortar", {
    description = S("Forest Floor Mortar\nCollects and randomly throws bombs"),
    tiles = {"bb_nodes_floor_with_mortar_forest.png", "bb_nodes_floor_forest.png"},
    groups = {cracky = 3, floor = 1, mortar = 1, solid = 1},
    paramtype = "light",
    paramtype2 = "facedir",
})

core.register_node(":bb_nodes:bush", {
    description = S("Bush (Forest Brick)"),
    paramtype = "light",
    drawtype = "mesh",
    mesh = "bush.obj",
    paramtype2 = "facedir",
    tiles = {"bush.png"},
    collision_box = {
        type = "fixed",
        fixed = {
            { -0.5, -.5, -0.5, 0.5, 2.0, 0.5 },
            { -0.5, -.5, -0.5, 0.5, .5, 0.5 },
        } -- Makes it 2 nodes high
    },
    groups = {cracky = 3, penetrable = 1, removable = 1, powerup = 1, solid = 1},
})

core.register_node(":bb_nodes:acorn", {
    description = S("Acorn (Forest Crate)"),
    tiles = {"acorn_top.png", "acorn_bottom.png", "acorn_side.png"},
    drawtype = "nodebox",
    node_box = {
        type = "fixed",
        fixed = {
            {-0.4375, -0.3750, -0.4375, 0.4375, 0.3750, 0.4375},
            {-0.3125, -0.5000, -0.3125, 0.3125, -0.3750, 0.3125},
            {-0.3750, 0.3750, -0.3750, 0.3750, 0.4375, 0.3750}
        }
    },
    collision_box = {
        type = "fixed",
        fixed = {
            { -0.5, -.5, -0.5, 0.5, 2.0, 0.5 },
            { -0.5, -.5, -0.5, 0.5, .5, 0.5 },
        } -- Makes it 2 nodes high
    },
    paramtype = "light",
    groups = {cracky = 3, penetrable = 1, removable = 1, powerup = 2, solid = 1},
})

core.register_node(":bb_nodes:mud", {
    description = S("Mud (Forest Ice)"),
    tiles = {"mud_top.png", "mud_top.png", "mud_side.png"},
    paramtype = "light",
    use_texture_alpha = "blend",
    drawtype = "nodebox",
    node_box = {
        type = "fixed",
        fixed = {
            {-0.5000, -0.5000, -0.5000, 0.5000, 0.5-(1/16), 0.5000},
        },
    },
    groups = {cracky = 3, slippery = 9, floor = 1, solid = 1},
})

core.register_node(":bb_nodes:rock", {
    description = S("Rock (Forest Solid Wall)"),
    paramtype = "light",
    drawtype = "mesh",
    mesh = "rock.obj",
    paramtype2 = "facedir",
    tiles = {"rock.png"},
    collision_box = {
        type = "fixed",
        fixed = {
            { -0.5, -.5, -0.5, 0.5, 2.0, 0.5 },
            { -0.5, -.5, -0.5, 0.5, .5, 0.5 },
        } -- Makes it 2 nodes high
    },
    groups = {cracky = 3, penetrable = 1, solid = 1},
})

core.register_node(":bb_nodes:fence_forest", {
    description = S("Forest Fence"),
    paramtype = "light",
    drawtype = "mesh",
    mesh = "fence.obj",
    paramtype2 = "facedir",
    selection_box = {
        type = "fixed",
        fixed = {
            {-0.5000, -0.5000, 0.5000, 0.5000, 0.5000, 0.3750},
        },
    },
    collision_box = {
        type = "fixed",
        fixed = {
            {-0.5000, -0.5000, 0.5000, 0.5000, 0.5000, 0.3750},
            {-0.5000, -0.5000, 0.5000, 0.5000, 1.9, 0.3750},
        },
    },
    tiles = {"fence.png"},
    groups = {cracky = 3, penetrable = 1, solid = 1},
})

core.register_node(":bb_nodes:fence_forest_corner", {
    description = S("Forest Fence (corner)"),
    paramtype = "light",
    drawtype = "mesh",
    mesh = "fence_corner.obj",
    paramtype2 = "facedir",
    selection_box = {
        type = "fixed",
        fixed = {
            {-0.5000, -0.5000, 0.5000, 0.5000, 0.5000, 0.3750},
            {0.5000, 0.5000, -0.5000, 0.3750, -0.5000, 0.5000}
        },
    },
    collision_box = {
        type = "fixed",
        fixed = {
            {-0.5000, -0.5000, 0.5000, 0.5000, 0.5000, 0.3750},
            {0.5000, 0.5000, -0.5000, 0.3750, -0.5000, 0.5000},
            {-0.5000, -0.5000, 0.5000, 0.5000, 1.9, 0.3750},
            {0.5000, 1.9, -0.5000, 0.3750, -0.5000, 0.5000}
        },
    },
    tiles = {"fence.png"},
    groups = {cracky = 3, penetrable = 1, solid = 1},
})
