local S = core.get_translator(blockbomber.modname)

core.register_entity("block_bomber_mg:dead_player",{
    initial_properties = {
        physical = false,
        use_texture_alpha = true,        
        collisionbox = {-0.01, -0.01, -0.01, 0.01, 0.01, 0.01},
        visual = "sprite",
        textures = {"blank.png"},
        visual_size = {x = 0.01, y = 0.01, z = 0.01},
        static_save = false,
    },
    on_step = function(self, dtime, moveresult)
        local arenaID, arena = arena_lib.get_arena_by_name(blockbomber.modname, self._arena_name)
        if not (arena) or not (arena.in_game) then self.object:remove() return end

        if arena.match_id ~= self._match_id then
            self.object:remove() return
        end

        self._timer = self._timer + dtime
        if self._timer > 4 and self._falling then
            self.object:remove()
        end

    end,

    on_activate = function(self, staticdata, dtime)
        if staticdata ~= "" and staticdata ~= nil then
            local data = core.parse_json(staticdata) or {}
            if data._falling then
                self._falling = data._falling
            end
            if data._arena_name then
                self._arena_name = data._arena_name
            end
            if data._match_id then
                self._match_id = data._match_id
            end
        end
    end,
    _timer = 0,
    _falling = false,
    _arena_name = "",
    _match_id = 0,

})


core.register_entity("block_bomber_mg:character",{
    initial_properties = {
        visual = "mesh",
        mesh = "bb_character.b3d",
        textures = {"white.png","player_parts.png"},
        visual_size = {x = 4, y = 4},
        collisionbox = {-0.4, 0.0, -0.4, 0.4, 0.9,0.4},
        stepheight = 0.3,
        eye_height = 1.3,
        physical = false,
        collides_with_objects = false,
    },
    on_step = function(self, dtime, moveresult)
        local arenaID, arena = arena_lib.get_arena_by_name(blockbomber.modname, self._arena_name)
        if not (arena) or not (arena.in_game) then self.object:remove() return end

        if arena.match_id ~= self._match_id then
            self.object:remove() return
        end
    end,

    on_activate = function(self, staticdata, dtime)
        if staticdata ~= "" and staticdata ~= nil then
            local data = core.parse_json(staticdata) or {}
            if data._arena_name then
                self._arena_name = data._arena_name
            end
            if data._match_id then
                self._match_id = data._match_id
            end
        end
    end,
    _arena_name = "",
    _match_id = 0,
})


core.register_entity("block_bomber_mg:att",{
    initial_properties = {
        physical = false,
        use_texture_alpha = true,        
        collisionbox = {-0.01, -0.01, -0.01, 0.01, 0.01, 0.01},
        visual = "sprite",
        textures = {"blank.png"},
        visual_size = {x = 0.01, y = 0.01, z = 0.01},
        static_save = false,
    },
    on_step = function(self, dtime, moveresult)
        
        self._timer = self._timer + dtime
        if self._timer > 300 then
            self.object:remove()
        end
    
    end,

    _timer = 0,

})