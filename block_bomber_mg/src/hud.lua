local S = core.get_translator(blockbomber.modname)
local infohuds = {}

function blockbomber.add_info_hud(p_name)

    local player = core.get_player_by_name(p_name)
    if not player then return end
    local id = player:hud_add({
        hud_elem_type   = "text",
        number          = 0xE6482E,
        position        = { x = .97, y = .03},
        offset          = {x = 0,   y = 0},
        text            = S("Loading ..."),
        alignment       = {x = -1, y = 1},
        scale           = {x = 100, y = 100},
        size            = {x = 2 },
    })
    infohuds[p_name] = id

end

function blockbomber.remove_info_hud(p_name)
    local player = core.get_player_by_name(p_name)
    if not player then return end

    if infohuds[p_name] then
        player:hud_remove(infohuds[p_name])
        infohuds[p_name] = nil
    end
end

function blockbomber.set_info_hud(p_name, info)
    local player = core.get_player_by_name(p_name)
    if not player then return end

    if infohuds[p_name] then
        player:hud_change(infohuds[p_name],"text", info)
    end
end
