local S = core.get_translator(blockbomber.modname)
if core.get_modpath("player_api") then
    player_api.register_model("bb_character.b3d", {
        animation_speed = 30,
        textures = {"blank.png","blank.png"},
        animations = {
            -- Standard animations.
            stand     = {x = 0,   y = 40},
            lay       = {x = 0,   y = 40},
            walk      = {x = 60, y = 100},
            mine      = {x = 0,   y = 40},
            walk_mine = {x = 60, y = 100},
            sit       = {x = 0,   y = 40},
        },
        collisionbox = {-0.4, 0.0, -0.4, 0.4, 0.9,0.4},
        stepheight = 0.3,
        eye_height = 1.3,
    })
    
end
