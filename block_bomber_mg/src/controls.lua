local S = core.get_translator(blockbomber.modname)

local try_to_place_bomb = function(player) end
local get_count_in_inv = function (player,item) end

controls.register_on_press(function(player, control_name)
    local placekey = "sneak"
    if control_name ~= placekey then return end
    local p_name = player:get_player_name()
    local mod = arena_lib.get_mod_by_player(p_name)
    if not mod then return end
    local arena = arena_lib.get_arena_by_player(p_name)
    if not arena.in_game then return end
    if arena.in_loading then return end
    if not arena.players[p_name] then return end

    try_to_place_bomb(player)
end)


controls.register_on_press(function(player, control_name)
    if not (control_name == "left" or control_name == "right" or control_name == "up" or control_name == "down") then
        return
    end

    local p_name = player:get_player_name()
    local mod = arena_lib.get_mod_by_player(p_name)
    if not mod then return end
    local arena = arena_lib.get_arena_by_player(p_name)
    if not arena.in_game then return end

    local inv = player:get_inventory()
    if inv:contains_item("main", "bb_powerup:weird_control") then
        local look = player:get_look_horizontal()
        player:set_look_horizontal(look - 3.14)
    end

end)


-- implement party here because try_to_place_bomb is here
local partytimer = 0
core.register_globalstep(function(dtime)
    partytimer = partytimer + dtime
    if partytimer > .5 then
        partytimer = 0
        for arenaID, arena in pairs(arena_lib.mods[blockbomber.modname].arenas) do
            if arena.in_game and not arena.in_loading and not arena.in_celebration then
                for p_name, stats in pairs(arena.players) do
                    local player = core.get_player_by_name(p_name)


                    -- partaaae!

                    local inv = player:get_inventory()
                    if inv:contains_item("main", "bb_powerup:party") then
                        try_to_place_bomb(player)
                    end
                end
            end
        end
    end
end)


function try_to_place_bomb(player)
    --get our vars
    local p_name = player:get_player_name()
    local inv = player:get_inventory()
    local pos = vector.add(player:get_pos(),vector.new(0,.5,0))

    -- booleans
    -- note: bomb throwing is handled in bomb code
    local can_placebomb = false
    if inv:contains_item("main", "bb_powerup:extra_bomb") and not inv:contains_item("main", "bb_powerup:no_placebomb") then
        can_placebomb = true
    end

    -- if there is a bomb that the player has just placed (they are standing on it) and they have throw ability, then throw it

    local near_bomb = false
    local pos1 = vector.add(pos, vector.new(-.5,0,-.5))
    local pos2 = vector.add(pos, vector.new(.5,0,.5))

    local objs = core.get_objects_in_area(pos1, pos2)
    for k,v in ipairs(objs) do
        if v and not ( v:is_player() ) and v:get_luaentity() and v:get_luaentity().name == "block_bomber_mg:bomb" then
            near_bomb = true
        end
    end


    if can_placebomb and near_bomb ~= true then

        -- place bomb, remove bomb item from inv
        local power = get_count_in_inv(player,"bb_powerup:extra_power") 

        local multidir = false
        if inv:contains_item("main", ItemStack('bb_powerup:multidir')) then
            multidir = true
            for k,v in pairs(inv:get_list("main")) do 
                if v:get_name() == 'bb_powerup:multidir' then
                    
                    v:add_wear((65535/3)+3)
                    inv:set_stack("main",k,v)
                end
            end
        end


        local staticdata = core.write_json({
            _power = power,
            _owner = p_name,
            _multidir = multidir,
        })

        core.sound_play({
            name = "sfx_sounds_impact4",
            gain = 0.6,
        }, {
            pos = player:get_pos(),
            max_hear_distance = 10,  
        }, true)

        core.add_entity(vector.round(pos), "block_bomber_mg:bomb", staticdata)

        inv:remove_item("main", "bb_powerup:extra_bomb")

    end

end


function get_count_in_inv(player,item)
    local count = 0
    local inv = player:get_inventory()
    local lists = inv:get_lists()
    for listname,itemstacklist in pairs(lists) do
        for _,stack in pairs( itemstacklist ) do
            if stack:get_name() == item then
                count = count + stack:get_count()
            end
        end
    end
    return count
end