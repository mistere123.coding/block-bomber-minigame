local S = core.get_translator(blockbomber.modname)
arena_lib.register_editor_section(blockbomber.modname, {
    name = "Set Origin",
    icon = "blbmr_icon.png",
    give_items = function(itemstack, user, arena)
        return {"block_bomber_mg:editor_set_origin"}
    end
})


core.register_craftitem("block_bomber_mg:editor_set_origin", {
	description = S("Set origin"),
	inventory_image = "bb_set_origin.png",
	wield_image = "bb_set_origin.png",
	groups = { not_in_creative_inventory = 1 },
	stack_max = 1,
	on_drop = function() end,
	on_place = function() end,

	on_use = function(itemstack, user, pointed_thing)
		local meta = user:get_meta()
		local p_name = user:get_player_name()
		local mod = meta:get_string("arena_lib_editor.mod")
		local arena_name = meta:get_string("arena_lib_editor.arena")
		local id, arena = arena_lib.get_arena_by_name(mod, arena_name)

        local pos = vector.apply(user:get_pos(),math.floor)
        arena_lib.change_arena_property(p_name, mod, arena_name, "origin", dump(pos), true)
    end,
})


arena_lib.on_enable(blockbomber.modname, function(arena, p_name)
    if not (arena.pos1 and arena.pos2) then
        core.chat_send_player(p_name,S("Arena area not set!"))
        return false
    end
    if not ((math.abs(arena.pos2.x - arena.pos1.x) > 30) and (math.abs(arena.pos2.z - arena.pos1.z) > 30) ) then
        core.chat_send_player(p_name,S("Arena area must be bigger than 30 on each side!"))
        return false
    end

    if vector.equals(vector.copy(arena.origin),vector.zero()) then
        -- get a vector from arena.pos1 and arena.pos2 that represents the point closest to the origin (or farthest in the nether regious of the coordinate system), then inset it by 10
        local x = math.min(arena.pos1.x,arena.pos2.x)
        local y = math.min(arena.pos1.y,arena.pos2.y)
        local z = math.min(arena.pos1.z,arena.pos2.z)
        local ten = vector.new(x,y,z) + vector.new(10,10,10)
        arena_lib.change_arena_property(p_name, blockbomber.modname, arena.name, "origin", dump(ten), true)
    end
    return true
end)


