local S = core.get_translator(blockbomber.modname)

core.register_abm({
    label = "suddendeath",
    nodenames = {"group:floor"},
    interval = .5,
    chance = .25,
    action = function(pos, node, active_object_count, active_object_count_wider)
        local arena = arena_lib.get_arena_by_pos(pos,blockbomber.modname)
        if not arena then return end
        if not arena.in_game then return end
        if not arena.sudden_death then return end


        local sec_since_sd = arena.sudden_death_timer
        if math.random(1,2000) < sec_since_sd then
            local objs = core.get_objects_inside_radius(pos, .4)
            local bomb_isthere = false
            for _,obj in pairs(objs) do
                if (not (obj:is_player())) and obj:get_luaentity().name == "block_bomber_mg:bomb" then
                    bomb_isthere = true
                end
            end
            if bomb_isthere == true then return end
            pos = vector.add(pos,vector.new(0,1,0))
            local power = 1 --math.random(1,3)
            local multidir = false
            local staticdata = core.write_json({
                _power = power,
                _multidir = multidir,
            })
            core.add_entity(pos, "block_bomber_mg:bomb", staticdata)
        end
    end,
})