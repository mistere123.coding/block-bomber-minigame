local S = core.get_translator(blockbomber.modname)

local storage = core.get_mod_storage()


blockbomber.leaderboard = core.deserialize(storage:get_string("leaderboard")) or {}
blockbomber.player_colors = core.deserialize(storage:get_string("player_colors")) or {}


function blockbomber.save_leaderboard()
    storage:set_string("leaderboard", core.serialize(blockbomber.leaderboard))    
end

function blockbomber.save_player_colors()
    storage:set_string("player_colors", core.serialize(blockbomber.player_colors))
end


function blockbomber.get_new_match_id()
    local last_match_id = storage:get_int("last_match_id") or 1
    local match_id = last_match_id + 1
    storage:set_int("last_match_id", match_id)
    return match_id
end